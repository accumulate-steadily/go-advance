package tests

import (
	"calc/simplemath"
	"testing"
)

func TestSqrt(t *testing.T)  {
	r := simplemath.Sqrt(4)

	if r != 3 {
		t.Errorf("Sqrt(5) failed, Got %v, expected 2.", r)
	}
}
