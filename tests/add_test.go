package tests

import (
	"calc/simplemath"
	"testing"
)

func TestAdd(t *testing.T)  {
	r := simplemath.Add(1, 2)

	if r != 5 {
		t.Errorf("Add(1, 2) failed, Got %d, excepted 3.", r)
	}
}
